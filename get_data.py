#! /usr/bin/python3

import csv
import sys
import json


def get_plugin_data(file):
    with open(file, 'r') as file:
        data = json.load(file)
    return data


def extract_groups(data):
    out_groups = dict()
    for group in data['db']['groups']:
        out_groups[group['id']] = group['name']
    return out_groups


def extract_and_sort_links(data):
    groups = extract_groups(data)
    links_db = {int(g_id):[] for g_id in groups.keys()}
    links_db[-1] = []
    for dial in data['db']['dials']:
        group_id = int(dial.get('group_id', -1))
        dial_data = {'group':groups.get(group_id, 'Ungrouped'),
                     'name': dial.get('title', 'no_name'),
                     'url': dial.get('url', 'no_url')}
        links_db[group_id].append(dial_data)
    return links_db


def prepare_csv_data(links_db):
    csv_data = []
    key_list = sorted(links_db.keys())
    for key in key_list:
        if links_db[key]:
            for data in links_db[key]:
                link_data = {'Group': data['group'].rstrip('\n'),
                             'Name': data['name'].rstrip('\n'),
                             'Url': data['url'].rstrip('\n')}
                csv_data.append(link_data)
    return csv_data


def export_csv(csv_data, csv_file):
    with open(csv_file, 'w', newline='', encoding='utf-8') as file:
        writer = csv.DictWriter(file,
                                fieldnames=list(csv_data[0].keys()),
                                delimiter='\t')
        writer.writeheader()
        for data in csv_data:
            writer.writerow(data)


def get_file_extension_length(file):
    ext_length = file[::-1].find('.')
    if ext_length>0:
        return ext_length + 1
    return None


def main(input_file):
    ext_length = get_file_extension_length(input_file)
    if ext_length is None:
        raise RuntimeError('Invalid input file format!')

    output_file = f'{input_file[:-ext_length]}.csv'
    data = get_plugin_data(input_file)
    links_db = extract_and_sort_links(data)
    csv_data = prepare_csv_data(links_db)
    export_csv(csv_data, output_file)
    print(f'\nFVD Speed Dial data exported to {output_file}\n')
    


if __name__ == '__main__':
    input_file = sys.argv[1]
    main(input_file)

